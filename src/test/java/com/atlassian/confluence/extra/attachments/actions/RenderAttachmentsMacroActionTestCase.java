package com.atlassian.confluence.extra.attachments.actions;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.xml.stream.XMLStreamException;
import java.util.List;

import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

public class RenderAttachmentsMacroActionTestCase extends TestCase
{
    private RenderAttachmentsMacroAction renderAttachmentsMacroAction;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private PermissionManager permissionManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        renderAttachmentsMacroAction = new RenderAttachmentsMacroAction()
        {
            @Override
            public String getText(String s)
            {
                return s;
            }
        };

        renderAttachmentsMacroAction.setPermissionManager(permissionManager);
        renderAttachmentsMacroAction.setXhtmlContent(xhtmlContent);
        renderAttachmentsMacroAction.setPage(new Page());
    }

    @Override
    protected void tearDown() throws Exception
    {
        permissionManager = null;
        xhtmlContent = null;
        super.tearDown();
    }

    public void testErrorMessageShownIfUserNotPermittedToViewTargetPage()
    {
        renderAttachmentsMacroAction.validate();
        assertTrue(renderAttachmentsMacroAction.hasActionErrors());
    }

    public void testMacroRenderedWithOldParameter() throws XMLStreamException, XhtmlException
    {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=true|upload=false}"), Matchers.<ConversionContext>anyObject(), Matchers.<List<RuntimeException>>anyObject())).thenReturn("");
        renderAttachmentsMacroAction.setOld(true);
        
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }

    public void testMacroRenderedWithSortByParameter() throws XMLStreamException, XhtmlException
    {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=false|upload=false|sortBy=date}"), Matchers.<ConversionContext>anyObject(), Matchers.<List<RuntimeException>>anyObject())).thenReturn("");

        renderAttachmentsMacroAction.setSortBy("date");
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }

    public void testMacroRenderedWithPatternsParameter() throws XMLStreamException, XhtmlException
    {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=false|upload=false|patterns=^.*\\.txt$}"), Matchers.<ConversionContext>anyObject(), Matchers.<List<RuntimeException>>anyObject())).thenReturn("");

        renderAttachmentsMacroAction.setPatterns("^.*\\.txt$");
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }

    public void testMacroWithAllSettableParameters() throws XMLStreamException, XhtmlException
    {
        when(xhtmlContent.convertWikiToView(eq("{attachments:old=true|upload=false|sortBy=date|patterns=^.*\\.txt$}"), Matchers.<ConversionContext>anyObject(), Matchers.<List<RuntimeException>>anyObject())).thenReturn("");

        renderAttachmentsMacroAction.setOld(true);
        renderAttachmentsMacroAction.setSortBy("date");
        renderAttachmentsMacroAction.setPatterns("^.*\\.txt$");
        assertEquals("", renderAttachmentsMacroAction.getRenderedMacroHtml());
    }
}
