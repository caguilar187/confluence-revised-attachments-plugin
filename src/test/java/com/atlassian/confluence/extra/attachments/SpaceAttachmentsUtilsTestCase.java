package com.atlassian.confluence.extra.attachments;

import junit.framework.TestCase;

import org.mockito.Mock;

import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.search.v2.SearchManager;

public class SpaceAttachmentsUtilsTestCase extends TestCase
{
	
	@Mock
	private SearchManager searchManager;
	
	@Mock
	private AnyTypeDao anyTypeDao;
	
	private DefaultSpaceAttachmentsUtils defaultSpaceAttachmentsUtils;
	
	public void setUp() throws Exception
	{
		defaultSpaceAttachmentsUtils = new DefaultSpaceAttachmentsUtils(searchManager, anyTypeDao);
	}

	public void testCalculateTotalPage()
	{
		assertEquals(3, defaultSpaceAttachmentsUtils.calculateTotalPage(26, 10));
		assertEquals(3, defaultSpaceAttachmentsUtils.calculateTotalPage(30, 10));
	}
	
	public void testCalculateStartIndex()
	{
		assertEquals(10, defaultSpaceAttachmentsUtils.calculateStartIndex(2, 10));
		assertEquals(30, defaultSpaceAttachmentsUtils.calculateStartIndex(3, 15));
	}
	
}
