package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.*;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.*;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.impl.DefaultUser;
import com.opensymphony.webwork.ServletActionContext;
import junit.framework.TestCase;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AttachmentsMacroTestCase extends TestCase
{
    @Mock
    private VelocityHelperService velocityHelperService;

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private PermissionManager permissionManager;

    private AttachmentsMacro attachmentsMacro;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private PageManager pageManager;
    
    @Mock
    private I18NBeanFactory i18NBeanFactory;
    
    @Mock
    private LocaleManager localeManager;
    
    @Mock
    private UserAccessor userAccessor;
    
    @Mock
    private FormatSettingsManager formatSettingsManager;

    private Page pageToBeRendered;

    private ConfluenceUser remoteUser;

    @Mock
    private ImagePreviewRenderer imagePreviewRenderer;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        ServletActionContext.setRequest(httpServletRequest);

        remoteUser = new ConfluenceUserImpl(new DefaultUser("admin"));
        AuthenticatedUserThreadLocal.set(remoteUser);

        pageToBeRendered = new Page();
        pageToBeRendered.setId(1);

        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(new HashMap<String, Object>());
        when(userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.getUser())).thenReturn(new ConfluenceUserPreferences());
        when(formatSettingsManager.getTimeFormat()).thenReturn("h:mm a");
        when(formatSettingsManager.getDateFormat()).thenReturn("MMM dd, yyyy");
        when(formatSettingsManager.getDateTimeFormat()).thenReturn("MMM dd, yyyy HH:mm");
        attachmentsMacro = new AttachmentsMacro(velocityHelperService, permissionManager, attachmentManager, pageManager, i18NBeanFactory, localeManager, formatSettingsManager, userAccessor, imagePreviewRenderer);
    }

    protected void tearDown() throws Exception
    {
        velocityHelperService = null;
        permissionManager = null;
        attachmentManager = null;
        httpServletRequest = null;
        ContainerManager.getInstance().setContainerContext(null);
        AuthenticatedUserThreadLocal.setUser(null);
        ServletActionContext.setRequest(null);
        super.tearDown();
    }

    private Attachment createAttachment(final String name, final long size, final Date lastModificationDate)
    {
        final Attachment attachment = new Attachment(name, "text/plain", size, StringUtils.EMPTY);

        attachment.setId(name.hashCode());
        attachment.setLastModificationDate(lastModificationDate);
        return attachment;
    }

    public void testWithUnsortedAttachments() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>(), "", pageToBeRendered.toPageContext());

        verifyTestResult(attachments);
    }

    public void testWithUnsortedAttachmentsWhileShowingOldAttachmentsAndAllowingUpload() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("old", Boolean.TRUE.toString());
                put("upload", Boolean.TRUE.toString());
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTestResult(attachments);
    }

    public void testCommentOwnerContentProcessedInsteadOfTheCommentItselfWhenMacroIsUsedInComment() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");
        final Comment comment = new Comment();

        comment.setOwner(pageToBeRendered);

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>(), "", comment.toPageContext());

        verifyTestResult(attachments);
    }

    public void testAttachmentsSortedIfSortByParameterIsNotValid() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(httpServletRequest.getParameter("sortBy")).thenReturn("invalidSortBy");
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>(), "", pageToBeRendered.toPageContext());

        Collections.reverse(attachments);
        verifyTestResult(attachments);
    }

    public void testWithAttachmentsSortedByName() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "name");
            }
        }, "", pageToBeRendered.toPageContext());

        Collections.reverse(attachments);
        verifyTestResult(attachments);
    }
    
    public void testWithAttachmentsSortedByNameDescending() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "name");
                put("sortOrder","descending");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTestResult(attachments);
    }

    public void testWithAttachmentsSortedBySize() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt");

        attachments.get(0).setFileSize(30);
        attachments.get(1).setFileSize(50);
        
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "size");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTestResult(attachments);
    }

    public void testWithAttachmentsSortedBySizeDescending() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        attachments.get(0).setFileSize(30);
        attachments.get(1).setFileSize(50);

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "size");
                put("sortOrder", "descending"); 
            }
        }, "", pageToBeRendered.toPageContext());

        Collections.reverse(attachments);
        verifyTestResult(attachments);
    }

    public void testWithAttachmentsSortedByLastModifiedDate() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "date");
            }
        }, "", pageToBeRendered.toPageContext());

        verifyTestResult(attachments);
    }

    public void testWithAttachmentsSortedByLastModifiedDateDescending() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "date");
                put("sortOrder", "descending"); 
            }
        }, "", pageToBeRendered.toPageContext());

        Collections.reverse(attachments);
        verifyTestResult(attachments);
    }
    
    public void testSortParameterFromHttpRequestPreferredOverMacroParameter() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file0.txt", "file1.txt");

        when(httpServletRequest.getParameter("sortBy")).thenReturn("name");
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);

        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("sortBy", "date");
                put("sortOrder", "ascending");
            }
        }, "", pageToBeRendered.toPageContext());

        Collections.reverse(attachments);
        verifyTestResult(attachments);
    }

    public void testAttachmentsCanBeFilteredByCommaSeparatedRegexes() throws MacroException
    {
        List<Attachment> attachments = createAttachments("file1.txt", "file0.txt", "file2.txt");

        when(httpServletRequest.getParameter("sortBy")).thenReturn("name");
        when(attachmentManager.getLatestVersionsOfAttachments(pageToBeRendered)).thenReturn(attachments);
        when(permissionManager.hasCreatePermission(remoteUser, pageToBeRendered, Attachment.class)).thenReturn(true);
        attachmentsMacro.execute(new HashMap<String, String>()
        {
            {
                put("patterns", "^file0\\.txt$,^file1\\.txt$");
            }
        }, "", pageToBeRendered.toPageContext());

        attachments.remove(2);
        verifyTestResult(attachments);
    }

    private void verifyTestResult(final List<Attachment> attachments)
    {
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object o)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, Object> contextMap = (Map<String, Object>) o;

                                return attachments.equals(contextMap.get("latestVersionsOfAttachments"))
                                        && (Boolean) contextMap.get("hasAttachFilePermissions")
                                        && pageToBeRendered.equals(contextMap.get("page"))
                                        && attachmentsMacro.equals(contextMap.get("macro"))
                                        && (Boolean) contextMap.get("old")
                                        && (Boolean) contextMap.get("upload")
                                        && 5 == ((Integer) contextMap.get("max"))
                                        && remoteUser.equals(contextMap.get("remoteUser"))
                                        && attachmentsMacro.equals(contextMap.get("attachmentsMacroHelper"));
                            }
                        }
                )
        );
    }

    private List<Attachment> createAttachments(String... names)
    {
        List<Attachment> result = new ArrayList<Attachment>();

        final Calendar now = Calendar.getInstance();
        for (String name : names)
        {
            Attachment attachment = createAttachment(name, 0, now.getTime());
            result.add(attachment);
            now.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    public void testAttachmentFileNameXmlEscapedByGetAttachmentDetails()
    {
        final Attachment attachment = createAttachment("'\"&<>", 0, new Date()); /* Attachment with XML unsafe characters */

        assertTrue(
                ArrayUtils.isEquals(
                        new String[] { StringEscapeUtils.escapeXml(attachment.getFileName()), String.valueOf(attachment.getVersion()) },
                        attachmentsMacro.getAttachmentDetails(attachment)
                )
        );
    }

    public void testMacroGeneratesBlockHtml()
    {
        assertEquals(Macro.OutputType.BLOCK, attachmentsMacro.getOutputType());
    }

    public void testMacroHasNoBody()
    {
        assertEquals(Macro.BodyType.NONE, attachmentsMacro.getBodyType());
    }

    public void testMacroDoesNotRenderBody()
    {
        assertEquals(RenderMode.NO_RENDER, attachmentsMacro.getBodyRenderMode());
    }
    
}
