package it.com.atlassian.confluence.extra.attachments.pageobjects;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.confluence.util.collections.Range.range;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * This drives the on-page representation of the {@link com.atlassian.confluence.extra.attachments.AttachmentsMacro}
 */
public class AttachmentsMacroComponent extends ConfluenceAbstractPageComponent
{
    private final Page page;

    public AttachmentsMacroComponent(Page page)
    {
        this.page = page;
    }

    @ElementBy(cssSelector = "table.attachments")
    PageElement attachmentsTable;

    @ElementBy(cssSelector = "table.attachments .filename-column a")
    PageElement filenameHeaderLink;

    @ElementBy(cssSelector = "table.attachments .modified-column a")
    PageElement modifiedHeaderLink;

    @ElementBy(cssSelector = ".download-all-link")
    PageElement downloadAllLink;

    @Init
    public void assertAttachmentsTableHeaders()
    {
        waitUntilTrue(filenameHeaderLink.timed().isVisible());
        waitUntilTrue(modifiedHeaderLink.timed().isVisible());
    }

    public PageElement getDownloadAllLink()
    {
        return downloadAllLink;
    }

    public URL getDownloadAllURL() throws MalformedURLException
    {
        return new URL(getDownloadAllLink().getAttribute("href"));
    }

    public AttachmentsMacroComponent clickSortByFilename()
    {
        filenameHeaderLink.click();
        return this;
    }

    public AttachmentsMacroComponent clickSortByModifiedDate()
    {
        modifiedHeaderLink.click();
        return this;
    }

    public AttachmentsMacroComponent assertAttachmentRowIsDisplayed(Attachment attachment, User lastModifiedBy)
    {
        getAttachmentRow(attachment).assertIsDisplayed(lastModifiedBy);
        return this;
    }

    public AttachmentsMacroComponent assertAttachmentRowIsNotDisplayed(Attachment attachment)
    {
        getAttachmentRow(attachment).assertIsNotVisible();
        return this;
    }

    public void assertAttachmentOrder(Attachment... expectedAttachmentOrder)
    {
        final List<Integer> expectedOrder = newArrayList(range(1, expectedAttachmentOrder.length + 1));
        waitUntilEquals("Unexpected attachment order", expectedOrder, getAttachmentOrder(expectedAttachmentOrder));
    }

    private TimedQuery<List<Integer>> getAttachmentOrder(final Attachment... attachments)
    {
        return Queries.forSupplier(new DefaultTimeouts(), new Supplier<List<Integer>>()
        {
            @Override
            public List<Integer> get()
            {
                final List<Integer> actualOrder = newArrayList();
                for (int i = 0; i < attachments.length; i++)
                {
                    Attachment attachment = attachments[i];
                    final int idx = getAttachmentRow(attachment).getAttachmentIndex();
                    actualOrder.add(idx);
                }
                return actualOrder;
            }
        });
    }

    public AttachmentSummary openAttachmentSummary(Attachment attachment)
    {
        return toggleAttachmentSummary(attachment).getAttachmentSummary(attachment);
    }

    public AttachmentsMacroComponent toggleAttachmentSummary(Attachment attachment)
    {
        getAttachmentRow(attachment).getAttachmentSummaryToggle().click();
        return this;
    }

    /**
     * To test the logic of the file-upload callback (i.e. that the attachments are re-rendered correctly, we don't
     * actually have to upload a file.
     * @param pageId the page to refresh the attachments macro for
     * @param expectedAttachment an attachment to wait for after the refresh
     */
    public AttachmentsMacroComponent refreshAttachments(long pageId, Attachment expectedAttachment)
    {
        String js = String.format("AJS.trigger('attachment-macro.refresh', %d);", pageId);
        driver.executeScript(js);
        waitUntilEquals(expectedAttachment.getComment(), getFirstAttachmentComment());
        return this;
    }

    private TimedQuery<String> getFirstAttachmentComment()
    {
        return Queries.forSupplier(new DefaultTimeouts(), new Supplier<String>()
        {
            @Override
            public String get()
            {
                List<WebElement> commentElements = driver.findElements(By.className("attachment-comment"));
                if (commentElements.isEmpty())
                {
                    return null; // no attachments yet, so no comments
                }
                return commentElements.get(0).getText();
            }
        });
    }

    public AttachmentRow getAttachmentRow(Attachment attachment)
    {
        final PageElement row = attachmentsTable
                .find(By.cssSelector("tr[data-attachment-id='" + attachment.getId() + "']"));
        return pageBinder.bind(AttachmentRow.class, attachment, row);
    }

    public AttachmentSummary getAttachmentSummary(Attachment attachment)
    {
        return pageBinder.bind(
                AttachmentSummary.class,
                attachmentsTable.find(By.cssSelector("tr.attachment-summary-" + attachment.getId())),
                attachment, page);
    }

    public static class AttachmentRow extends ConfluenceAbstractPageComponent
    {
        private final Attachment attachment;
        private final PageElement container;

        public AttachmentRow(Attachment attachment, PageElement container)
        {
            this.attachment = attachment;
            this.container = container;
        }

        private PageElement getAttachmentSummaryToggle()
        {
            return container.find(By.cssSelector(".attachment-summary-toggle span"));
        }

        public PageElement getAttachmentDownloadLink()
        {
            return container.find(By.cssSelector("a.filename"));
        }

        public URL getAttachmentDownloadUrl() throws MalformedURLException
        {
            return new URL(getAttachmentDownloadLink().getAttribute("href"));
        }

        int getAttachmentIndex()
        {
            return Integer.valueOf(container.getAttribute("data-attachment-idx"));
        }

        public void assertIsNotVisible()
        {
            assertThat("Attachment [" + attachment.getFilename() + "] should not be present", container.isPresent(), is(false));
        }

        public void assertIsDisplayed(User lastModifiedBy)
        {
            final PageElement filenameLink = getAttachmentDownloadLink();
            final PageElement comment = container.find(By.className("attachment-comment"));
            final PageElement modified = container.find(By.className("attachment-created"));

            waitUntilEquals(attachment.getFilename(), filenameLink.timed().getText());
            waitUntilEquals(attachment.getComment(), comment.timed().getText());
            waitUntil(modified.timed().getText(), endsWith(lastModifiedBy.getDisplayName()));
        }
    }
}