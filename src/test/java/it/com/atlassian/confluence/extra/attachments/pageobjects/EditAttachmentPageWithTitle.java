package it.com.atlassian.confluence.extra.attachments.pageobjects;

import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.pageobjects.page.content.EditAttachmentPage;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * A custom subclass of the standard {@link com.atlassian.confluence.pageobjects.page.content.EditAttachmentPage} which adds assertions we need for this test.
 * Should ideally be part of the original class.
 */
public class EditAttachmentPageWithTitle extends EditAttachmentPage
{
    private final Attachment attachment;

    public EditAttachmentPageWithTitle(Attachment attachment)
    {
        super(attachment);
        this.attachment = attachment;
    }

    public void assertTitleMatchesAttachment()
    {
        assertThat(driver.getTitle(), allOf(
                containsString("Properties"),
                containsString(attachment.getFilename())
        ));
    }
}
