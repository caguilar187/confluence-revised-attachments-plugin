package it.com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static java.io.File.createTempFile;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.lang.RandomStringUtils.random;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.log4j.lf5.util.StreamUtils.copy;

public class AttachmentsMacroTestCase extends AbstractConfluencePluginWebTestCase
{
    private static final String ATTACHMENTS_TABLE = "//div[@class='plugin_attachments_table_container']//table[@class='attachments aui']";
    private static final String ATTACHMENTS_TABLE_BODY = ATTACHMENTS_TABLE + "//tbody";
    private static final String ATTACHMENTS_TABLE_HEAD = ATTACHMENTS_TABLE + "//thead";

    private static final String FIRST_ATTACHMENT_LINK = ATTACHMENTS_TABLE_BODY + "//tr[@class='attachment-row']/td[2]//a";
    private static final String NAME_COLUMN_HEADER = ATTACHMENTS_TABLE_HEAD + "//th[2]//a";
    private static final String MODIFIED_COLUMN_HEADER = ATTACHMENTS_TABLE_HEAD + "//th[3]//a";

    private long createTestPage(String spaceKey, String pageTitle, String wikiMarkup)
    {
        PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setTitle(pageTitle);
        pageHelper.setContent(wikiMarkup);

        assertTrue(pageHelper.create());

        return pageHelper.getId();
    }

    private void createTestSpace(String spaceKey, String spaceName)
    {
    	 SpaceHelper spaceHelper = getSpaceHelper();

         spaceHelper.setKey(spaceKey);
         spaceHelper.setName(spaceName);

         assertTrue(spaceHelper.create());
    }


    private long createAttachment(long pageId, String filename, String contentType, byte[] content, String comment)
    {
        AttachmentHelper attachmentHelper = new AttachmentHelper(getConfluenceWebTester());

        attachmentHelper.setParentId(pageId);
        attachmentHelper.setFileName(filename);
        attachmentHelper.setContentType(contentType);
        attachmentHelper.setContent(content);
        attachmentHelper.setContentLength(content.length);

        assertTrue(attachmentHelper.create());

        if (isNotBlank(comment))
        {
            gotoPage("/pages/editattachment.action?pageId=" + pageId + "&fileName=" + filename);
            setWorkingForm("editattachment");
            setTextField("newComment", comment);
            submit("confirm");
        }

        return attachmentHelper.getId();
    }

    private long createAttachment(long pageId, String filename, String contentType, byte[] content)
    {
        return createAttachment(pageId, filename, contentType, content, EMPTY);
    }

    private void viewPage(long pageId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + pageId);
    }

    public void testAttachmentsFilteredByRegex() throws IOException
    {
        long testPageId = createTestPage("ds", getName(), "{attachments:patterns=^shouldnotseeanyattachments.txt$}");

        createAttachment(testPageId, "test.txt", "text/plain", "Test content".getBytes("UTF-8"));
        viewPage(testPageId);

        assertEquals(
                "No files shared here yet.",
                getElementTextByXPath("//div[@class='drop-zone-empty-text']")
        );
    }

    public void testFileUpload() throws IOException
    {
        long testPageId = createTestPage("ds", getName(), "{attachments:upload=true}");

        File tempFile = createTempFile("testFileUpload", ".txt");
        writeByteArrayToFile(tempFile, random(32).getBytes("UTF-8"));

        viewPage(testPageId);

        setWorkingForm(1);
        setTextField("file_0", tempFile.getAbsolutePath());
        submit("confirm");

        assertEquals(
                tempFile.getName(),
                getElementAttributByXPath(FIRST_ATTACHMENT_LINK, "data-filename")
        );

        assertTrue(tempFile.delete());
    }

    private File copyClassPathResourceToFile(String classpathResource, String fileNameSuffix) throws IOException
    {
        InputStream in = null;
        OutputStream out = null;

        try
        {
            File tempFile = createTempFile("it.com.atlassian.confluence.extra.attachments", fileNameSuffix);

            in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream(classpathResource));
            out = new BufferedOutputStream(new FileOutputStream(tempFile));

            copy(in, out);
            return tempFile;
        }
        finally
        {
            closeQuietly(out);
            closeQuietly(in);
        }
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/CONFATT-4">CONFATT-4</a>
     * @throws IOException
     * Thrown if there is an error copying class path resource &quot;CONFATT-4-site-export.zip&quot; to the temporary
     * directory for the data restore operation.
     */
    public void testOpenWebFolderLinkNotShownIfAttachmentsStoredInWebdavServer() throws IOException
    {
        File confatt4SiteExport = copyClassPathResourceToFile("CONFATT-4-site-export.zip", ".zip");

        try
        {
            /* Restore a site backup that contains a global settings with the WebDAV server URL set */
            getConfluenceWebTester().restoreData(confatt4SiteExport);

            long testPageId = createTestPage("ds", getName(), "{attachments}");

            viewPage(testPageId);
            assertLinkNotPresentWithText("Open Web Folder");
        }
        finally
        {
            assertTrue(confatt4SiteExport.delete());
        }
    }

    /// This is important for CONFATT-6
    public void testErrorDivPresentOnConfluenceBuiltInAttachmentsUploadPageIfThereWereErrors()
    {
        long testPageId = createTestPage("ds", getName(), "");

        gotoPage("/pages/viewpageattachments.action?pageId=" + testPageId);

        setWorkingForm("upload-attachments");
        submit();

        assertElementPresentByXPath("//div[@class='aui-message error']");
    }

    public void testViewAttachmentsFromOtherPage() throws IOException
    {
    	final String testContentString = "Test content";
    	long attachmentsMacroPageId = createTestPage("ds", "Page With Attachments Macro", "{attachments:page=testViewAttachmentsFromOtherPage}");
    	long testPageId = createTestPage("ds", getName(), "");
    	createAttachment(testPageId, "test.txt", "text/plain", testContentString.getBytes("UTF-8"));

        viewPage(attachmentsMacroPageId);

        assertAttachmentsTableColumns();

        assertEquals("test.txt", getElementAttributByXPath(FIRST_ATTACHMENT_LINK, "data-filename"));
    }

    public void testViewAttachmentsFromOtherSpace() throws IOException
    {
    	final String testContentString = "Test content";

    	createTestSpace("TST", "Test Space");
    	long attachmentsMacroPageId = createTestPage("ds", "Page With Attachments Macro", "{attachments:page=TST:testViewAttachmentsFromOtherSpace}");
    	long testPageId = createTestPage("TST", getName(), "");
    	createAttachment(testPageId, "test.txt", "text/plain", testContentString.getBytes("UTF-8"));

        viewPage(attachmentsMacroPageId);

        assertAttachmentsTableColumns();

        assertEquals("test.txt", getElementAttributByXPath(FIRST_ATTACHMENT_LINK, "data-filename"));
    }

    public void testLongFilenamesNotTruncated() throws IOException
    {
        final String longAttachmentName = "The quick brown fox jumps over the lazy dog and she sells sea shells on the sea shore.txt";
        long testPageId = createTestPage("ds", getName(), "{attachments}");

        createAttachment(testPageId, longAttachmentName, "text/plain", "Test content".getBytes("UTF-8"));
        viewPage(testPageId);

        assertLinkPresentWithExactText(longAttachmentName);
    }

    private void assertAttachmentsTableColumns()
    {
        assertEquals("File", getElementTextByXPath(NAME_COLUMN_HEADER));
        assertEquals("Modified", getElementTextByXPath(MODIFIED_COLUMN_HEADER));
    }

}
