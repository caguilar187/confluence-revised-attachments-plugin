package it.com.atlassian.confluence.extra.attachments;

import java.io.IOException;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;

public class SpaceAttachmentsMacroTestCase extends AbstractConfluencePluginWebTestCase
{
	private long createTestPage(String spaceKey, String pageTitle, String wikiMarkup)
	{
		PageHelper pageHelper = getPageHelper();

		pageHelper.setSpaceKey(spaceKey);
		pageHelper.setTitle(pageTitle);
		pageHelper.setContent(wikiMarkup);

		assertTrue(pageHelper.create());

		return pageHelper.getId();
	}

	private void createTestSpace(String spaceKey, String spaceName)
	{
		SpaceHelper spaceHelper = getSpaceHelper();

		spaceHelper.setKey(spaceKey);
		spaceHelper.setName(spaceName);

		assertTrue(spaceHelper.create());
	}


	private long createAttachment(long pageId, String filename, String contentType, byte[] content)
	{
		AttachmentHelper attachmentHelper = getAttachmentHelper();

		attachmentHelper.setParentId(pageId);
		attachmentHelper.setFileName(filename);
		attachmentHelper.setContentType(contentType);
		attachmentHelper.setContent(content);
		attachmentHelper.setContentLength(content.length);

		assertTrue(attachmentHelper.create());

		return attachmentHelper.getId();
	}
	private void viewPage(long pageId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + pageId);
    }
	
	public void testNoAttachmentMessageShownWhenThereIsNoAttachmentToSpace() throws IOException
	{
		createTestSpace("TST", "Test Space");
		long spaceAttachmentsMacroPageId = createTestPage("TST", "testNoAttachmentMessageShownWhenThereIsNoAttachmentToSpace", "{space-attachments:space=TST}");
	    viewPage(spaceAttachmentsMacroPageId);
	   
	    assertEquals(
                "There are no attachments in the TST space.",
                getElementTextByXPath("//div[@class='wiki-content']//div[@class='greybox']")
        );
	}
	
	public void testShowSpaceAttachments() throws IOException
	{
		final String testContentString = "Test content";
		createTestSpace("TST", "Test Space");
		
    	long testPageId = createTestPage("TST", "testShowSpaceAttachments", "");
    	createAttachment(testPageId, "test1.txt", "text/plain", testContentString.getBytes("UTF-8"));
    	assertTrue(getIndexHelper().flush());
    	
        long spaceAttachmentsMacroPageId = createTestPage("TST", "Page With Space Attachments Macro", "{space-attachments:space=TST}");
    	viewPage(spaceAttachmentsMacroPageId);
    	
    	assertEquals("test1.txt", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[1]//td[1]//a[2]"));
    	assertEquals("testShowSpaceAttachments", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//td[@class='attachedto']//a"));
    	
	}
	
	public void testSpaceAttachmentsAttachedToCorrectPage() throws IOException
	{
		final String testContentString = "Test content";
		createTestSpace("TST", "Test Space");
		
    	long testPageId = createTestPage("TST", "Page 1", "");
    	createAttachment(testPageId, "test1.txt", "text/plain", testContentString.getBytes("UTF-8"));
    	
    	long testPageTwoId = createTestPage("TST", "Page 2", "");
    	createAttachment(testPageTwoId, "test2.txt", "text/plain", testContentString.getBytes("UTF-8"));
    	
    	assertTrue(getIndexHelper().flush());
    	
        long spaceAttachmentsMacroPageId = createTestPage("TST", "testSpaceAttachmentsAttachedToCorrectPage", "{space-attachments:space=TST}");
    	viewPage(spaceAttachmentsMacroPageId);
    	assertEquals("test2.txt", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[1]//td[1]//a[2]"));
    	assertEquals("Page 2", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[1]//td[@class='attachedto']//a"));
    	assertEquals("test1.txt", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[2]//td[1]//a[2]"));
    	assertEquals("Page 1", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[2]//td[@class='attachedto']//a"));
	}
	
	public void testAttachmentFromOtherSpaceNotShow() throws IOException
	{
		final String testContentString = "Test content";
		createTestSpace("TST", "Test Space");
		
    	long testPageId = createTestPage("TST", "Page in Test Space", "");
    	createAttachment(testPageId, "test1.txt", "text/plain", testContentString.getBytes("UTF-8"));
    	
    	long testPageTwoId = createTestPage("ds", "Page in Demonstration Space", "");
    	createAttachment(testPageTwoId, "test2.txt", "text/plain", testContentString.getBytes("UTF-8"));
    	
    	assertTrue(getIndexHelper().flush());
    	
        long spaceAttachmentsMacroPageId = createTestPage("TST", "testAttachmentFromOtherSpaceNotShow", "{space-attachments:space=TST}");
    	viewPage(spaceAttachmentsMacroPageId);
    	assertEquals("test1.txt", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[1]//td[1]//a[2]"));
    	assertEquals("Page in Test Space", getElementTextByXPath("//div[@class='attachments-container']//table[@class='attachments aui']//tbody//tr[1]//td[@class='attachedto']//a"));
    	assertLinkPresentWithExactText("test1.txt");
    	assertLinkNotPresentWithExactText("test2.txt");
	}
	
}
