package it.com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.plugin.Plugin;
import com.atlassian.confluence.it.plugin.SimplePlugin;
import com.atlassian.confluence.pageobjects.page.content.ViewPage;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.atlassian.confluence.extra.attachments.pageobjects.AttachmentSummary;
import it.com.atlassian.confluence.extra.attachments.pageobjects.AttachmentsMacroComponent;
import it.com.atlassian.confluence.extra.attachments.test.MacroParameter;
import it.com.atlassian.confluence.extra.attachments.test.MacroParametersRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.confluence.it.SpacePermission.*;
import static it.com.atlassian.confluence.extra.attachments.AttachmentDownloadTestUtils.downloadAndVerifyAttachmentsZip;
import static it.com.atlassian.confluence.extra.attachments.AttachmentDownloadTestUtils.fetchUrlAndCheckContent;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AttachmentsMacroWebDriverTest extends AbstractWebDriverTest
{
    @Rule public TestName testName = new TestName();
    @Rule public MacroParametersRule macroParametersRule = new MacroParametersRule();

    private Page page;

    private static final Plugin WORKBOX_COMMON = new SimplePlugin("com.atlassian.mywork.mywork-common-plugin", "");
    private static final Plugin WORKBOX_PROVIDER = new SimplePlugin("com.atlassian.mywork.mywork-confluence-provider-plugin", "");
    private static final Plugin WORKBOX_HOST = new SimplePlugin("com.atlassian.mywork.mywork-confluence-host-plugin", "");
    private static final List<Plugin> WORKBOX_PLUGINS = Arrays.asList(WORKBOX_COMMON, WORKBOX_PROVIDER, WORKBOX_HOST);

    private static boolean workboxDisabled = false;

    @Before
    public void setUp() throws Exception
    {
        rpc.logIn(User.ADMIN);

        // CONFDEV-16033 Temporary hack. Just disable the workbox to get around deadlocks when test data is removed.
        if (!workboxDisabled)
        {
            for (Plugin plugin : WORKBOX_PLUGINS)
            {
                if (rpc.getPluginHelper().isPluginEnabled(plugin))
                {
                    rpc.getPluginHelper().disablePlugin(plugin);
                    workboxDisabled = true;
                }
            }
        }

        rpc.createUser(User.TEST2);

        rpc.grantPermissions(Space.TEST, User.TEST, ATTACHMENT_CREATE, ATTACHMENT_REMOVE);
        rpc.grantPermissions(Space.TEST, User.TEST2, ATTACHMENT_CREATE);
        rpc.grantPermissions(Space.TEST, User.TEST2, VIEW);

        page = createPageWithAttachmentsMacro();
    }

    @Test
    public void emptyAttachmentsTable()
    {
        loginAndViewAttachmentsMacro();
    }

    @Test
    public void singleAttachmentIsDisplayed()
    {
        final Attachment attachment = createTextAttachment();
        loginAndViewAttachmentsMacro()
                .assertAttachmentRowIsDisplayed(attachment, User.TEST)
                .openAttachmentSummary(attachment)
                .assertNoLabels()
                .assertAttachmentVersionIsDisplayed(1, true, User.TEST, "Test file");
    }

    @Test
    public void multipleAttachmentVersions()
    {
        rpc.logIn(User.TEST2);
        final Attachment version1 = createTextAttachment();

        final Attachment version2 = new Attachment(version1, "I've changed", "For the better".getBytes());
        rpc.logIn(User.TEST);
        rpc.createAttachment(version2);

        final Attachment version3 = new Attachment(version1, "I've changed once more", "Not so good now".getBytes());
        rpc.logIn(User.TEST2);
        rpc.createAttachment(version3);

        loginAndViewAttachmentsMacro()
                .assertAttachmentRowIsDisplayed(version3, User.TEST2)
                .openAttachmentSummary(version1)
                .assertAttachmentVersionIsDisplayed(1, false, User.TEST2, "Test file")
                .assertAttachmentVersionIsDisplayed(2, false, User.TEST, "I've changed")
                .assertAttachmentVersionIsDisplayed(3, true, User.TEST2, "I've changed once more");
    }

    @Test
    public void attachmentLabelsAreDisplayedAndCanBeEdited()
    {
        final Attachment attachment = createTextAttachment();

        final AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment);

        attachmentSummary
                .openLabelsDialog()
                .addLabel("my-label")
                .close();

        attachmentSummary.assertDisplayedLabels("my-label");
    }

    // CONFDEV-17289
    @Test
    public void attachmentLabelsHaveCorrectUrl()
    {
        final Attachment attachment = createTextAttachment();

        final AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro().openAttachmentSummary(attachment);

        attachmentSummary.openLabelsDialog()
                         .addLabel("my-label")
                         .close();

        attachmentSummary.assertDisplayedLabels("my-label");

        String expectedUrl = product.getProductInstance().getBaseUrl() + "/label/" + page.getSpace().getKey() + "/my-label";

        viewAttachmentsMacro().openAttachmentSummary(attachment)
                              .assertDisplayedLabelsWithUrl(expectedUrl);
    }

    @Test
    public void displayPreviewImage()
    {
        final String filename = "image.png";

        Attachment attachment = createAttachment(filename, "image/png");
        AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment);
        assertThat(attachmentSummary.hasImagePreview(filename), is(true));
    }

    @Test
    public void displayPreviewForPdf()
    {
        final String filename = "slides.pdf";

        Attachment attachment = createAttachment(filename, "application/pdf");
        AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment);
        assertThat(attachmentSummary.hasPdfPreview(filename), is(true));
    }

    @Test
    public void multipleAttachmentSummaries()
    {
        rpc.logIn(User.TEST);
        final Attachment file1 = createTextAttachment("test1.txt");
        rpc.logIn(User.TEST2);
        final Attachment file2 = createTextAttachment("test2.txt");

        final AttachmentsMacroComponent attachments = loginAndViewAttachmentsMacro();
        attachments.assertAttachmentRowIsDisplayed(file1, User.TEST);
        attachments.assertAttachmentRowIsDisplayed(file2, User.TEST2);

        attachments.toggleAttachmentSummary(file1);

        attachments.getAttachmentSummary(file1).assertIsVisible();
        attachments.getAttachmentSummary(file2).assertIsNotVisible();

        attachments.toggleAttachmentSummary(file2);

        attachments.getAttachmentSummary(file1).assertIsVisible();
        attachments.getAttachmentSummary(file2).assertIsVisible();

        attachments.toggleAttachmentSummary(file1);

        attachments.getAttachmentSummary(file1).assertIsNotVisible();
        attachments.getAttachmentSummary(file2).assertIsVisible();
    }

    @Test
    public void attachmentOrderingByNameAscending()
    {
        final Attachment alice = createTextAttachment("alice.txt");
        final Attachment carol = createTextAttachment("carol.txt");
        final Attachment bob = createTextAttachment("Bob.txt");

        loginAndViewAttachmentsMacro()
                .clickSortByFilename()
                .assertAttachmentOrder(alice, bob, carol);
    }

    @Test
    public void attachmentOrderingByModifiedDateDescending()
    {
        final Attachment alice = createTextAttachment("alice.txt");
        final Attachment carol = createTextAttachment("carol.txt");
        final Attachment bob = createTextAttachment("Bob.txt");

        loginAndViewAttachmentsMacro()
                .clickSortByModifiedDate()  // Clicking this will make the direction reverse, as we have Modified Date (ascending) order by default
                .assertAttachmentOrder(bob, carol, alice);
    }

    @Test
    public void openAttachmentPropertiesEditor()
    {
        final Attachment attachment = createTextAttachment();

        loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment)
                .clickAttachmentProperties()
                .assertTitleMatchesAttachment();
    }

    @Test
    public void deleteAttachment()
    {
        final Attachment attachment = createTextAttachment();

        loginAndViewAttachmentsMacro()
                .openAttachmentSummary(attachment)
                .clickRemoveAttachment()
                .assertDialogTitleAndContentMatchesAttachment()
                .closeDialog()
                .getAttachmentSummary(attachment)
                .clickRemoveAttachment()
                .confirm();

        viewAttachmentsMacro().assertAttachmentRowIsNotDisplayed(attachment);
    }

    @Test
    public void deleteAttachmentAfterUpload()
    {
        AttachmentsMacroComponent macroComponent = loginAndViewAttachmentsMacro();

        // 'Fake' upload a new attachment version and trigger the refresh
        final Attachment attachment = createTextAttachment();
        macroComponent.refreshAttachments(page.getId(), attachment);

        macroComponent.openAttachmentSummary(attachment)
                .clickRemoveAttachment()
                .confirm();

        viewAttachmentsMacro().assertAttachmentRowIsNotDisplayed(attachment);
    }

    @Test
    public void cannotDeleteAttachmentWithoutPermission()
    {
        final Attachment attachment = createTextAttachment();

        loginAndViewAttachmentsMacro(User.TEST2)
                .openAttachmentSummary(attachment)
                .assertRemoveAttachmentLinkNotPresent();
    }

    @Test
    @MacroParameter(name = "old", value = "false")
    public void oldVersionsNotDisplayed()
    {
        final Attachment attachment = createTextAttachment();
        final Attachment version2 = new Attachment(attachment, "I've changed", "For the better".getBytes());
        rpc.createAttachment(version2);

        loginAndViewAttachmentsMacro()
                .assertAttachmentRowIsDisplayed(version2, User.TEST)
                .openAttachmentSummary(attachment)
                .assertVersionHistoryNotPresent();
    }

    // CONFDEV-15288
    @Test
    public void oldVersionsAreDisplayedAfterUpload()
    {
        final Attachment attachment = createTextAttachment();

        // First assert that we see the history on page load
        final ViewPage viewPage = product.login(User.TEST, ViewPage.class, page);
        AttachmentsMacroComponent macroComponent = getAttachmentsMacroComponent(viewPage);
        macroComponent.assertAttachmentRowIsDisplayed(attachment, User.TEST)
                .openAttachmentSummary(attachment)
                .assertVersionHistoryPresent();

        // 'Fake' upload a new attachment version and trigger the refresh
        final Attachment version2 = new Attachment(attachment, "I've changed", "For the better".getBytes());
        rpc.createAttachment(version2);
        macroComponent.refreshAttachments(page.getId(), version2);

        // Version history should be present on the newly-refreshed Attachment line
        macroComponent = getAttachmentsMacroComponent(viewPage);
        macroComponent.openAttachmentSummary(attachment)
            .assertVersionHistoryPresent();
    }

    @Test
    public void downloadAllLinkOnlyPresentForTwoOrMoreAttachments()
    {
        assertThat(loginAndViewAttachmentsMacro().getDownloadAllLink().isPresent(), is(false));

        createTextAttachment("test1.txt");
        assertThat(viewAttachmentsMacro().getDownloadAllLink().isPresent(), is(false));

        createTextAttachment("test2.txt");
        assertThat(viewAttachmentsMacro().getDownloadAllLink().isPresent(), is(true));
    }

    @Test
    public void downloadLatestAttachmentVersion() throws IOException
    {
        final Attachment attachment1 = createTextAttachment("test1.txt", "comment 1", "file 1 contents");
        final Attachment attachment2 = createTextAttachment("test2.txt", "comment 2", "file 2 contents");

        final AttachmentsMacroComponent attachments = loginAndViewAttachmentsMacro();
        fetchUrlAndCheckContent("file 1 contents", attachments.getAttachmentRow(attachment1).getAttachmentDownloadUrl());
        fetchUrlAndCheckContent("file 2 contents", attachments.getAttachmentRow(attachment2).getAttachmentDownloadUrl());
    }

    @Test
    public void downloadOldAttachmentVersion() throws IOException
    {
        final Attachment attachment = createTextAttachment("test.txt", "comment", "original file contents");
        rpc.createAttachment(new Attachment(attachment, "I've changed", "For the better".getBytes()));

        final AttachmentSummary attachmentSummary = loginAndViewAttachmentsMacro().openAttachmentSummary(attachment);

        fetchUrlAndCheckContent("original file contents", attachmentSummary.getVersion(1)
                                                                           .getAttachmentVersionDownloadUrl());
        fetchUrlAndCheckContent("For the better", attachmentSummary.getVersion(2)
                .getAttachmentVersionDownloadUrl());
    }

    @Test
    public void downloadAllAttachmentsZip() throws IOException
    {
        final Attachment attachment1 = createTextAttachment("test1.txt", "comment 1", "file 1 contents");
        final Attachment attachment2 = createTextAttachment("test2.txt", "comment 2", "file 2 contents");

        final URL downloadUrl = loginAndViewAttachmentsMacro().getDownloadAllURL();

        downloadAndVerifyAttachmentsZip(downloadUrl, attachment1, attachment2);
    }


    private AttachmentsMacroComponent loginAndViewAttachmentsMacro()
    {
        return loginAndViewAttachmentsMacro(User.TEST);
    }

    private AttachmentsMacroComponent loginAndViewAttachmentsMacro(User user)
    {
        final ViewPage viewPage = product.login(user, ViewPage.class, page);
        return getAttachmentsMacroComponent(viewPage);
    }

    private AttachmentsMacroComponent viewAttachmentsMacro()
    {
        final ViewPage viewPage = product.visit(ViewPage.class, page);
        return getAttachmentsMacroComponent(viewPage);
    }

    private AttachmentsMacroComponent getAttachmentsMacroComponent(ViewPage viewPage)
    {
        return viewPage.getComponent(AttachmentsMacroComponent.class, page);
    }

    private Attachment createTextAttachment()
    {
        return createTextAttachment("test.txt");
    }

    private Attachment createTextAttachment(String filename)
    {
        return createAttachment(filename, "text/plain", "Test file", "testing");
    }

    private Attachment createTextAttachment(String filename, String comment, String content)
    {
        return createAttachment(filename, "text/plain", comment, content);
    }

    private Attachment createAttachment(String filename, String contentType)
    {
        return createAttachment(filename, contentType, "Test file", "testing");
    }

    private Attachment createAttachment(String filename, String contentType, String comment, String content)
    {
        Attachment attachment = new Attachment(page, filename, contentType, comment, content.getBytes());
        rpc.createAttachment(attachment);

        return attachment;
    }

    private Page createPageWithAttachmentsMacro()
    {
        rpc.logIn(User.TEST);
        Page page = new Page(Space.TEST, testName.getMethodName(), buildAttachmentMacroDescriptor(macroParametersRule.getMacroParameter()));
        rpc.createPage(page);
        return page;
    }

    private static String buildAttachmentMacroDescriptor(MacroParameter macroParameter)
    {
        String paramString = "";
        if (macroParameter != null)
        {
            paramString = String.format(":%s=%s", macroParameter.name(), macroParameter.value());
        }
        return String.format("{attachments%s}", paramString);
    }

}