package it.com.atlassian.confluence.extra.attachments.pageobjects;

import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.confluence.pageobjects.component.dialog.LabelsDialog;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import java.net.MalformedURLException;
import java.net.URL;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Represents the component on the attachments macro output which renders the expandable summary for a given attachment
 */
public class AttachmentSummary extends ConfluenceAbstractPageComponent
{
    private final PageElement container;
    private final Attachment attachment;
    private final Page page;

    public AttachmentSummary(PageElement container, Attachment attachment, Page page)
    {
        this.container = container;
        this.attachment = attachment;
        this.page = page;
    }

    public void assertIsVisible()
    {
        waitUntilTrue(container.timed().isVisible());
    }

    public void assertIsNotVisible()
    {
        waitUntilFalse(container.timed().isVisible());
    }

    private PageElement getEditLabelLink()
    {
        return container.find(By.cssSelector("a.show-labels-editor"));
    }

    private PageElement getRemoveAttachmentLink()
    {
        return container.find(By.className("removeAttachmentLink"));
    }

    public EditAttachmentPageWithTitle clickAttachmentProperties()
    {
        container.find(By.className("editAttachmentLink")).click();
        return pageBinder.bind(EditAttachmentPageWithTitle.class, attachment);
    }

    public VersionRow getVersion(int version)
    {
        final PageElement versionRow = container.find(By.cssSelector("tr[data-attachment-version='" + version + "']"));
        waitUntilTrue(versionRow.timed().isVisible());
        return pageBinder.bind(VersionRow.class, versionRow, version);
    }

    private PageElement getImagePreview()
    {
        return container.find(By.cssSelector("div.attachment-image-preview"));
    }

    public boolean hasImagePreview(String filename)
    {
        final PageElement img = getImagePreview().find(By.tagName("img"));
        return img.getAttribute("src").contains(filename);
    }

    public boolean hasPdfPreview(String filename)
    {
        // search for old flash viewer or new html viewer
        final PageElement pageElement = getImagePreview().find(By.cssSelector("object#Slides, div.vf-slide-viewer-macro"));
        return pageElement.isVisible();
    }

    public AttachmentSummary assertVersionHistoryNotPresent()
    {
        final PageElement historyWrapper = container.find(By.className("attachment-history-wrapper"));

        assertThat(historyWrapper.isPresent(), is(false));

        return this;
    }

    public AttachmentSummary assertVersionHistoryPresent()
    {
        final PageElement historyWrapper = container.find(By.className("attachment-history-wrapper"));

        assertThat(historyWrapper.isPresent(), is(true));

        return this;
    }

    private PageElement getLabelsContent()
    {
        return container.find(By.cssSelector(".labels-content"));
    }

    public AttachmentRemovalConfirmationDialog clickRemoveAttachment()
    {
        getRemoveAttachmentLink().click();
        return pageBinder.bind(AttachmentRemovalConfirmationDialog.class, attachment, page);
    }

    public AttachmentSummary assertAttachmentVersionIsDisplayed(int version, boolean isCurrentVersion, final User user, final String expectedComment)
    {
        getVersion(version).assertAttachmentVersionIsDisplayed(isCurrentVersion, user, expectedComment);
        return this;
    }

    public AttachmentSummary assertNoLabels()
    {
        final PageElement noLabelsElement = getLabelsContent().find(By.cssSelector(".no-labels-message"));
        waitUntilEquals("No labels", noLabelsElement.timed().getText());
        return this;
    }

    public LabelsDialog openLabelsDialog()
    {
        getEditLabelLink().click();
        LabelsDialog dialog = pageBinder.bind(LabelsDialog.class);
        waitUntilTrue(dialog.isReady());
        return dialog;
    }

    public void assertDisplayedLabels(String expectedLabels)
    {
        final PageElement labelList = getLabelsContent().find(By.cssSelector(".aui-label"));
        waitUntilEquals(expectedLabels, labelList.timed().getText());
    }

    public void assertDisplayedLabelsWithUrl(String url)
    {
        final PageElement labelList = getLabelsContent().find(By.cssSelector(".aui-label a"));
        waitUntilEquals(url, labelList.timed().getAttribute("href"));
    }

    public void assertRemoveAttachmentLinkNotPresent()
    {
        assertThat(getRemoveAttachmentLink().isPresent(), is(false));
    }

    public static class VersionRow extends ConfluenceAbstractPageComponent
    {
        private final PageElement container;
        private final Integer version;

        public VersionRow(PageElement container, Integer version)
        {
            this.container = container;
            this.version = version;
        }

        PageElement getAttachmentVersionLabel()
        {
            return container.find(By.cssSelector(".attachment-version"));
        }

        PageElement getAttachmentVersionLink()
        {
            return getAttachmentVersionLabel().find(By.cssSelector("a"));
        }

        public URL getAttachmentVersionDownloadUrl() throws MalformedURLException
        {
            return new URL(getAttachmentVersionLink().getAttribute("href"));
        }

        private PageElement getVersionModifiedBy()
        {
            return container.find(By.cssSelector(".version-modified-by"));
        }

        private PageElement getVersionComment()
        {
            return container.find(By.cssSelector(".version-comment"));
        }

        void assertAttachmentVersionIsDisplayed(boolean isCurrentVersion, final User user, final String expectedComment)
        {
            assertEquals("Version " + version, getAttachmentVersionLink().getText());
            if (isCurrentVersion)
            {
                assertEquals("Version " + version + " (current version)", getAttachmentVersionLabel()
                                                                                    .getText());
            }
            else
            {
                assertEquals("Version " + version, getAttachmentVersionLabel().getText());
            }

            if (version == 1)
            {
                assertEquals("Created by " + user.getDisplayName(), getVersionModifiedBy().getText());
            }
            else
            {
                assertEquals("Modified by " + user.getDisplayName(), getVersionModifiedBy().getText());
            }
            assertEquals(expectedComment, getVersionComment().getText());
        }
    }
}
