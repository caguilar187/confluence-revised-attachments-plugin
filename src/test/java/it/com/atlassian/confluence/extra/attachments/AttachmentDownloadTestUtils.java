package it.com.atlassian.confluence.extra.attachments;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.RestHelper;
import com.atlassian.confluence.it.User;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import static com.sun.jersey.api.client.config.ClientConfig.PROPERTY_FOLLOW_REDIRECTS;
import static java.io.File.createTempFile;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.apache.commons.io.IOUtils.toByteArray;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

class AttachmentDownloadTestUtils
{

    private static final Pattern JSESSIONID_PATTERN = Pattern.compile("(.*)(;jsessionid=\\w*)(.+)");

    static void downloadAndVerifyAttachmentsZip(URL downloadUrl, Attachment... attachments) throws IOException
    {
        WebResource downloadResource = RestHelper.newResource(downloadUrl.toExternalForm(), User.TEST);
        downloadResource.getProperties().put(PROPERTY_FOLLOW_REDIRECTS, false);

        try
        {
            downloadResource.get(Void.class);
        }
        catch (UniformInterfaceException ex)
        {
            // This happens because when we try to fetch the "all attachments" zip file via Jersey like this,
            // the jsessionid gets appended to the URL, and ExportDownloadResourceManager can't handle that, and refuses
            // the request. We need to capture the redirect, remove the jsessionid, and resend it with auth parameters added
            assertThat(ex.getResponse().getStatus(), is(302));
            downloadResource = RestHelper.newResource(extractRedirectUrl(ex.getResponse()), User.TEST);

            final byte[] data = downloadResource.get(byte[].class);

            File tmpFile = createTempFile(AttachmentsMacroWebDriverTest.class.getName(), ".zip");
            tmpFile.deleteOnExit();
            writeByteArrayToFile(tmpFile, data);

            for (Attachment attachment : attachments)
            {
                assertZipFileEntryContent(tmpFile, attachment);
            }
        }
    }

    private static void assertZipFileEntryContent(File tmpFile, Attachment attachment) throws IOException
    {
        final ZipFile zipFile = new ZipFile(tmpFile);

        final ZipEntry zipEntry = zipFile.getEntry(attachment.getFilename());
        final byte[] contents = toByteArray(zipFile.getInputStream(zipEntry));

        assertThat(new String(contents, "UTF-8"), is(new String(attachment.getData(), "UTF-8")));

        zipFile.close();
    }

    /**
     * Extract the redirect location from the given response, strip out the jsessionid (if any), and return.
     */
    private static String extractRedirectUrl(ClientResponse redirectResponse)
    {
        final String redirectLocation = redirectResponse.getHeaders().getFirst("Location");
        final Matcher jsessionIdMatcher = JSESSIONID_PATTERN.matcher(redirectLocation);
        if (jsessionIdMatcher.matches())
        {
            return jsessionIdMatcher.group(1) + jsessionIdMatcher.group(3);
        }
        else
        {
            return redirectLocation;
        }
    }

    static void fetchUrlAndCheckContent(String expectedContents, URL downloadUrl) throws MalformedURLException
    {
        assertThat(fetchUrl(downloadUrl), is(expectedContents));
    }

    private static String fetchUrl(URL attachmentDownloadUrl)
    {
        // Use RestHelper here to build our authenticated URL, even though we're not using REST.
        return RestHelper.newResource(attachmentDownloadUrl.toExternalForm(), User.TEST).get(String.class);
    }
}
