package it.com.atlassian.confluence.extra.attachments.pageobjects;

import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * We could use {@link com.atlassian.confluence.pageobjects.component.attachment.AttachmentRemovalConfirmationDialog} here,
 * but this version is more robust.
 */
public class AttachmentRemovalConfirmationDialog extends Dialog
{
    private final Attachment attachment;
    private final Page page;

    @ElementBy(className = "dialog-panel-body")
    PageElement dialogBody;

    @ElementBy(className = "dialog-title")
    PageElement dialogTitle;

    public AttachmentRemovalConfirmationDialog(Attachment attachment, Page page)
    {
        super("attachment-removal-confirm-dialog");
        this.attachment = attachment;
        this.page = page;
    }

    @Init
    @Override
    public void waitUntilVisible()
    {
        super.waitUntilVisible();
    }

    public AttachmentRemovalConfirmationDialog assertDialogTitleAndContentMatchesAttachment()
    {
        // TODO change the assert to equal "Attachment Deletion Confirmation" once confluence 5.2 becomes stable.
        assertThat(dialogTitle.getText(), anyOf(equalTo("Attachment Deletion Confirmation"),
                equalTo("Attachment Removal Confirmation")));
        assertThat(dialogBody.getText(), containsString(attachment.getFilename()));

        return this;
    }

    public AttachmentsMacroComponent closeDialog()
    {
        clickCancel();
        waitForRemoval();
        return pageBinder.bind(AttachmentsMacroComponent.class, page);
    }

    public void confirm()
    {
        clickButton("button-panel-submit-button", false);
        waitForRemoval();
    }
}
