package com.atlassian.confluence.extra.attachments.actions;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;

/**
 * This class lets the macro to render itself when a user changes the list of attachments being displayed by
 * uploading a new file.<br>
 * <br>
 * While Confluence does provide a similar action, there is no guarantee that its behavior of permission checking will
 * ever remain the same. So that's why this exists.
 */
public class RenderAttachmentsMacroAction extends ConfluenceActionSupport implements PageAware
{
    private AbstractPage page;

    private String sortBy;

    private String patterns;
    
    private String labels;

    private boolean old;

    private XhtmlContent xhtmlContent;

    public AbstractPage getPage()
    {
        return page;
    }

    public void setLabels(String labels)
    {
        this.labels = labels;
    }

    public void setPage(AbstractPage page)
    {
        this.page = page;
    }

    public void setSortBy(String sortBy)
    {
        this.sortBy = sortBy;
    }

    public void setPatterns(String patterns)
    {
        this.patterns = patterns;
    }

    public void setOld(boolean old)
    {
        this.old = old;
    }

    @SuppressWarnings("unused")
    public void setXhtmlContent(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    private String buildMacroMarkup()
    {
        StringBuilder marcroMarkupBuilder = new StringBuilder("{attachments")
                .append(":old=").append(old)
                .append("|upload=false");

        if (null != sortBy)
            marcroMarkupBuilder.append("|sortBy=").append(sortBy);
        if (null != patterns)
            marcroMarkupBuilder.append("|patterns=").append(patterns);
        if (!StringUtils.isBlank(labels))
            marcroMarkupBuilder.append("|labels=").append(labels);

        marcroMarkupBuilder.append("}");

        return marcroMarkupBuilder.toString();
    }

    public String getRenderedMacroHtml() throws XMLStreamException, XhtmlException
    {
        return xhtmlContent.convertWikiToView(buildMacroMarkup(), new DefaultConversionContext(getPage().toPageContext()), Lists.<RuntimeException>newArrayList());
    }

    @Override
    public void validate()
    {
        super.validate();

        if (!permissionManager.hasPermission(getRemoteUser(), Permission.VIEW, getPage()))
            addActionError(getText("not.permitted.description"));
    }

    public boolean isPageRequired()
    {
        return true;
    }

    public boolean isLatestVersionRequired()
    {
        return true;
    }

    public boolean isViewPermissionRequired()
    {
        return true;
    }

    public Space getSpace()
    {
        if (page != null)
            return page.getSpace();

        return null;
    }
}
