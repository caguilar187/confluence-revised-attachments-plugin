package com.atlassian.confluence.extra.attachments.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import bucket.core.actions.PaginationSupport;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.attachments.SpaceAttachments;
import com.atlassian.confluence.extra.attachments.SpaceAttachmentsUtils;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.LabelUtil;

public class SpaceAttachmentsAction extends ConfluenceActionSupport implements SpaceAware
{
	
	private SpaceAttachmentsUtils spaceAttachmentsUtils;
	private String sortBy;
	private String fileExtension;
	private String labelFilter;
	private String messageKey;
	private String[] messageParameter;
	private int pageNumber;
	private int totalAttachments;
	private int totalPage;
	private int pageSize;
	private List<Attachment> latestVersionsOfAttachments;
	private boolean allowFilterByFileExtension;
	private boolean showFilter;
	private boolean showAttachmentsNotFound;
	private Space space;
	private PaginationSupport<Attachment> paginationSupport = new PaginationSupport<Attachment>(SpaceAttachmentsUtils.COUNT_ON_EACH_PAGE);
	
	@Override
	public void validate()
	{
		super.validate();
		if (!LabelUtil.isValidLabelNames(getLabelFilter()))
		{
			setShowAttachmentsNotFound(false);
			addFieldError("attachmentLabelsString", getText(getText("attachments.labels.invalid.characters", new String[]{LabelParser.getInvalidCharactersAsString()})));
		}
	}
	
	@Override
	public String execute() throws InvalidSearchException
	{
		Set<String> labels = new HashSet<String>(getLabelsForFilter(getLabelFilter(), labelManager));
		SpaceAttachments spaceAttachments = null;
		
		if(labels.size() != 0 || StringUtils.isBlank(getLabelFilter()))
		{
		     spaceAttachments = spaceAttachmentsUtils.getAttachmentList(getSpace().getKey(), 
		        getPageNumber(), 
		        getTotalAttachments(), 
		        getPageSize(), getSortBy(), formatFileExtension(getFileExtension()), labels);
            setLatestVersionsOfAttachments(spaceAttachments.getAttachmentList());
            setTotalAttachments(spaceAttachments.getTotalAttachments());
            setTotalPage(spaceAttachments.getTotalPage());
		}
		setShowAttachmentsNotFound(true);
		
		if (spaceAttachments == null || spaceAttachments.getAttachmentList().isEmpty())
		{
			setMessages();
		}
		
		return SUCCESS;
	}
	
	public void setSpaceAttachmentsUtils(SpaceAttachmentsUtils spaceAttachmentsUtils)
    {
    	this.spaceAttachmentsUtils = spaceAttachmentsUtils;
    }

	public String getSpaceKey()
	{
		return getSpace().getKey();
	}

	public void setPageNumber(int pageNumber)
	{
		this.pageNumber = pageNumber;
	}

	public int getPageNumber()
	{
		return pageNumber;
	}

	public void setTotalAttachments(int totalAttachments)
	{
		this.totalAttachments = totalAttachments;
	}

	public int getTotalAttachments()
	{
		return totalAttachments;
	}

	public void setLatestVersionsOfAttachments(
			List<Attachment> latestVersionsOfAttachments)
	{
		this.latestVersionsOfAttachments = latestVersionsOfAttachments;
	}

	public List<Attachment> getLatestVersionsOfAttachments()
	{
		return latestVersionsOfAttachments;
	}

	public void setTotalPage(int totalPage)
	{
		this.totalPage = totalPage;
	}

	public int getTotalPage()
	{
		return totalPage;
	}

	public void setSortBy(String sortBy)
	{
		this.sortBy = sortBy;
	}

	public String getSortBy()
	{
		return sortBy;
	}

	public void setFileExtension(String fileExtension)
	{
		this.fileExtension = fileExtension;
	}

	public String getFileExtension()
	{
		return fileExtension;
	}

	public void setAllowFilterByFileExtension(boolean allowFilterByFileExtension)
	{
		this.allowFilterByFileExtension = allowFilterByFileExtension;
	}

	public boolean isAllowFilterByFileExtension()
	{
		return allowFilterByFileExtension;
	}

	@Override
	public boolean isSpaceRequired()
	{
		return true;
	}

	@Override
	public boolean isViewPermissionRequired()
	{
		return true;
	}

	@Override
	public void setSpace(Space space)
	{
		this.space = space;
	}

	@Override
	public Space getSpace()
	{
		return space;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setShowFilter(boolean showFilter)
	{
		this.showFilter = showFilter;
	}

	public boolean isShowFilter()
	{
		return showFilter;
	}
	
	private String formatFileExtension(String fileExtension)
	{
		if (StringUtils.isNotBlank(fileExtension))
		{
			if (fileExtension.startsWith("."))
			{
				fileExtension = fileExtension.substring(fileExtension.lastIndexOf(".") + 1, fileExtension.length());
			}
		}
		return fileExtension;
	}
	
	public PaginationSupport<Attachment> getPaginationSupport()
	{
		return paginationSupport;
	}

	public String getLabelFilter()
	{
		return labelFilter;
	}

	public void setLabelFilter(String labelFilter) 
	{
		this.labelFilter = labelFilter;
	}

	public boolean isShowAttachmentsNotFound()
	{
		return showAttachmentsNotFound;
	}

	public void setShowAttachmentsNotFound(boolean showAttachmentsNotFound) 
	{
		this.showAttachmentsNotFound = showAttachmentsNotFound;
	}

	public String getMessageKey() 
	{
		return messageKey;
	}
	
	public String[] getMessageParameter() 
	{
		return messageParameter;
	}

	private void setMessages() 
	{
		if (StringUtils.isNotBlank(getFileExtension()) && StringUtils.isNotBlank(getLabelFilter()))
		{
			this.messageKey = "attachments.no.attachments.with.label.and.ending.with.ext";
			this.messageParameter =  new String[]{ GeneralUtil.htmlEncode(getFileExtension()), GeneralUtil.htmlEncode(getLabelFilter()) };
		}
		else if (StringUtils.isNotBlank(getFileExtension()))
		{
			this.messageKey = "attachments.no.attachments.ending.with.ext";
			this.messageParameter = new String[]{ GeneralUtil.htmlEncode(getFileExtension()) };
		}
		else if (StringUtils.isNotBlank(getLabelFilter()))
		{
			this.messageKey = "attachments.no.attachments.with.label";
			this.messageParameter = new String[]{ GeneralUtil.htmlEncode(getLabelFilter()) };
		}
		else
		{
			this.messageKey = "attachments.no.attachments.to.space";
			this.messageParameter = new String[]{ GeneralUtil.htmlEncode(space.getKey()) };
		}
	}

	public static List<String> getLabelsForFilter(String labels, LabelManager labelManager)
    {
        List<String> labelsList = new ArrayList<String>();
        if (labels != null)
        {
            for (String labelString : LabelUtil.split(labels))
            {
                if (!StringUtils.isBlank(labelString))
                {
                    ParsedLabelName labelName = LabelParser.parse(labelString);
                    Label label = labelManager.getLabel(labelName);
                    if (label != null)
                        labelsList.add(label.getName());
                    else
                        return Collections.emptyList();
                }
            }
        }
        return labelsList;
    }
}
