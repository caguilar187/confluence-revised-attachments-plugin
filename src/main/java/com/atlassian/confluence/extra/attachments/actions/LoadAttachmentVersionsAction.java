package com.atlassian.confluence.extra.attachments.actions;


import java.util.List;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.plugin.descriptor.web.DefaultWebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;

import static com.google.common.collect.Lists.newArrayList;

public class LoadAttachmentVersionsAction extends ConfluenceActionSupport
{
    private AttachmentManager attachmentManager;
    private long attachmentId;
    private List<Attachment> allVersions;
    private Attachment currentVersion;

    public void setAttachmentManager(AttachmentManager attachmentManager)
    {
        this.attachmentManager = attachmentManager;
    }

    @Override
    public boolean isPermitted()
    {
        Attachment attachment = attachmentManager.getAttachment(attachmentId);
        return permissionManager.hasPermission(getAuthenticatedUser(), Permission.VIEW, attachment);
    }

    @Override
    public String execute() throws Exception
    {
        Attachment attachment = attachmentManager.getAttachment(attachmentId);
        if (attachment != null)
        {
            setCurrentVersion(attachment);

            final List<Attachment> allVersions = newArrayList();
            allVersions.addAll(getAllVersions(attachment));
            setAllVersions(allVersions);
        }
        return SUCCESS;
    }

    @Override
    public WebInterfaceContext getWebInterfaceContext()
    {
        DefaultWebInterfaceContext webInterfaceContext = new DefaultWebInterfaceContext(super.getWebInterfaceContext());
        webInterfaceContext.setAttachment(currentVersion);
        webInterfaceContext.setPage(getPage());
        return webInterfaceContext;
    }

    private List<Attachment> getAllVersions(Attachment attachment) throws Exception
    {
        return attachmentManager.getAllVersions(attachment);
    }

    public long getAttachmentId()
    {
        return attachmentId;
    }

    public void setAttachmentId(long attachmentId)
    {
        this.attachmentId = attachmentId;
    }

    public AbstractPage getPage()
    {
        return (AbstractPage) getCurrentVersion().getContent();
    }

    public List<Attachment> getAllVersions()
    {
        return allVersions;
    }

    public void setAllVersions(List<Attachment> allVersions)
    {
        this.allVersions = allVersions;
    }


    public Attachment getCurrentVersion()
    {
        return currentVersion;
    }

    public void setCurrentVersion(Attachment currentVersion)
    {
        this.currentVersion = currentVersion;
    }
}