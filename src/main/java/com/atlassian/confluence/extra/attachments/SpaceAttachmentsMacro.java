package com.atlassian.confluence.extra.attachments;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bucket.core.actions.PaginationSupport;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.core.datetime.FriendlyDateFormatter;
import com.atlassian.confluence.core.datetime.RequestTimeThreadLocal;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;

public class SpaceAttachmentsMacro extends BaseMacro implements Macro
{

	private static final Logger logger = LoggerFactory.getLogger(SpaceAttachmentsMacro.class);
	
	private static final String SPACE_KEY = "space";
	private static final String SHOW_FILTER_KEY = "showFilter";
	
	private final VelocityHelperService velocityHelperService;
	private final SpaceAttachmentsUtils spaceAttachmentsUtils;
	private final I18NBeanFactory i18NBeanFactory;
    private final LocaleManager localeManager;
    private final SpaceManager spaceManager;
    private final FormatSettingsManager formatSettingsManager;
    private final UserAccessor userAccessor;
    private PaginationSupport<Attachment> paginationSupport = new PaginationSupport<Attachment>(SpaceAttachmentsUtils.COUNT_ON_EACH_PAGE);
    
    public SpaceAttachmentsMacro(VelocityHelperService velocityHelperService, SpaceAttachmentsUtils spaceAttachmentsUtils, I18NBeanFactory i18NBeanFactory, 
    		LocaleManager localeManager, SpaceManager spaceManager, FormatSettingsManager formatSettingsManager, UserAccessor userAccessor)
    {
    	this.velocityHelperService = velocityHelperService;
    	this.spaceAttachmentsUtils = spaceAttachmentsUtils;
    	this.i18NBeanFactory = i18NBeanFactory;
    	this.localeManager = localeManager;
    	this.spaceManager = spaceManager;
    	this.formatSettingsManager = formatSettingsManager;
        this.userAccessor = userAccessor;
    }
	
	@Override
	public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
	{
		String spaceKey = getSpaceKey(conversionContext.getEntity(), parameters);
		boolean showFilter = Boolean.valueOf((StringUtils.isBlank(parameters.get(SHOW_FILTER_KEY)) ? "true" : parameters.get(SHOW_FILTER_KEY)));
		
		if(spaceManager.getSpace(spaceKey) == null || StringUtils.isBlank(spaceKey))
		{
			return RenderUtils.blockError(getI18NBean().getText("confluence.extra.attachments.error.spacenotfound",
    				Arrays.asList(GeneralUtil.htmlEncode(spaceKey))), "");
		}
        
		Map<String, Object> contextMap = velocityHelperService.createDefaultVelocityContext();
		
		try
		{
			SpaceAttachments spaceAttachments = spaceAttachmentsUtils.getAttachmentList(spaceKey, 1, 0, 0, "date", null, null);
            User remoteUser = AuthenticatedUserThreadLocal.getUser();
			contextMap.put("latestVersionsOfAttachments", spaceAttachments.getAttachmentList()); 
			contextMap.put("totalAttachments", spaceAttachments.getTotalAttachments());
			contextMap.put("totalPage", spaceAttachments.getTotalPage());
			contextMap.put("spaceKey", spaceKey);
			contextMap.put("title", conversionContext.getEntity().getTitle());
			contextMap.put("pageNumber", 1);
			contextMap.put("sortBy", "date");
			contextMap.put("showFilter", showFilter);
			contextMap.put("allowFilterByFileExtension", spaceAttachments.getAttachmentList() != null && !spaceAttachments.getAttachmentList().isEmpty());
			contextMap.put("paginationSupport", paginationSupport);
            contextMap.put("remoteUser", remoteUser);
            contextMap.put("showAttachmentsNotFound", true);
            if (spaceAttachments.getAttachmentList().isEmpty())
            {
            	 contextMap.put("messageKey", "attachments.no.attachments.to.space");
            	 contextMap.put("messageParameter", new String[]{ spaceKey });
            }
		
			ConfluenceUserPreferences pref = userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.getUser());
	        contextMap.put("friendlyDateFormatter", new FriendlyDateFormatter(RequestTimeThreadLocal.getTimeOrNow(), pref.getDateFormatter(formatSettingsManager, localeManager)));
	        
		} 
		catch (InvalidSearchException e)
		{
			throw new MacroExecutionException(e);
		}  
		
		return velocityHelperService.getRenderedTemplate("templates/extra/attachments/spaceattachments.vm", contextMap);
	}
	
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
	{
		try
		{
			return execute(parameters, body, new DefaultConversionContext(renderContext));
		}
		catch (MacroExecutionException e)
		{
			throw new MacroException(e);
		}
	}

	@Override
	public RenderMode getBodyRenderMode()
	{
		return RenderMode.NO_RENDER;
	}

	@Override
	public boolean hasBody()
	{
		return false;
	}

	@Override
	public BodyType getBodyType()
	{
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType()
	{
		return OutputType.BLOCK;
	}
	
	private I18NBean getI18NBean()
	{
		return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
	}
	
	private String getSpaceKey(ContentEntityObject contentObject, Map<String, String> params)
	{
		String spaceKey = (String) params.get(SPACE_KEY);
		if (StringUtils.isBlank(spaceKey))
		{
			if (contentObject instanceof SpaceContentEntityObject)
			{
				spaceKey = ((SpaceContentEntityObject) contentObject).getSpaceKey();
			}
			else if (contentObject instanceof Draft)
			{
				spaceKey = ((Draft) contentObject).getDraftSpaceKey();
			}
			else if (contentObject instanceof Comment)
			{
				ContentEntityObject commentOwner = ((Comment) contentObject).getOwner();
				spaceKey = getSpaceKey(commentOwner, params);
			}
		}

		if (spaceKey == null && logger.isDebugEnabled())
			logger.debug(String.format("Could not retrieve space key from content object: %s", contentObject));

		return spaceKey;
	}
	
}
