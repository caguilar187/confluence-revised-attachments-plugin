package com.atlassian.confluence.extra.attachments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.datetime.FriendlyDateFormatter;
import com.atlassian.confluence.core.datetime.RequestTimeThreadLocal;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import com.atlassian.confluence.plugin.descriptor.web.DefaultWebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.renderer.ConfluenceRenderContextOutputType;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.AttachmentComparator;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Implements the {attachments} macro.
 */
public class AttachmentsMacro extends BaseMacro implements Macro, EditorImagePlaceholder
{
    private final AttachmentManager attachmentManager;
    private final PermissionManager permissionManager;
    private final VelocityHelperService velocityHelperService;
    private final PageManager pageManager;
    private final I18NBeanFactory i18NBeanFactory;
    private final LocaleManager localeManager;
    private final FormatSettingsManager formatSettingsManager;
    private final UserAccessor userAccessor;
    private final ImagePreviewRenderer imagePreviewRenderer;
    private final String SORTORDER_ASCENDING = "ascending";
    private final String SORTORDER_DESCENDING = "descending";

    private ContentEntityObject contentObject;
    private PageContext pageContext;
    private ConversionContext conversionContext;
    private static final String PLACEHOLDER_IMAGE_PATH = "/download/resources/confluence.extra.attachments/placeholder.png";

    public AttachmentsMacro(VelocityHelperService velocityHelperService, PermissionManager permissionManager, AttachmentManager attachmentManager,
    		PageManager pageManager, I18NBeanFactory i18NBeanFactory, LocaleManager localeManager, FormatSettingsManager formatSettingsManager, 
    		UserAccessor userAccessor, ImagePreviewRenderer imagePreviewRenderer)
    {
        this.velocityHelperService = velocityHelperService;
        this.permissionManager = permissionManager;
        this.attachmentManager = attachmentManager;
        this.pageManager = pageManager;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
        this.formatSettingsManager = formatSettingsManager;
        this.userAccessor = userAccessor;
        this.imagePreviewRenderer = imagePreviewRenderer;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
    {
        return false;
    }

    // irrelevant since we have no body
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    /**
     * Process a request to render an {attachments} macro
     */
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        this.conversionContext = conversionContext;
        String pageTitle = parameters.get("page");
        if (StringUtils.isNotBlank(pageTitle))
        {
        	contentObject = getPage(conversionContext.getPageContext(), pageTitle);
        	pageContext = new PageContext(contentObject);
        	if (contentObject == null)
        	{
        		return RenderUtils.blockError(getI18NBean().getText("confluence.extra.attachments.error.pagenotfound",
        				Arrays.asList(GeneralUtil.htmlEncode(pageTitle))), "");
        	}

        	// check user permission 
        	if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, contentObject))
        		return RenderUtils.blockError(getI18NBean().getText("confluence.extra.attachments.error.nopagepermission",
        				Arrays.asList(GeneralUtil.htmlEncode(pageTitle))), "");
        }
        else
        {
	        // retrieve a reference to the body object this macro is in
	        contentObject = conversionContext.getEntity();
	        pageContext = conversionContext.getPageContext();
        }
        String sortBy = null;
        String sortOrder = null;

        // CONF-9989: if this macro is run from within a comment, use the underlying page to find the attachments
        if (contentObject instanceof Comment)
            contentObject = ((Comment) contentObject).getOwner();

        List<Attachment> filteredList = new ArrayList<Attachment>();

        // If the entities id == 0, the page object was newly created and is not persisted yet. Therefore does not have any attributes.
        if (contentObject != null && contentObject.getId() != 0)
        {
            sortBy = parameters.get("sortBy");
            sortOrder = parameters.get("sortOrder");
            // check if any sortBy parameters were set by the request if available.
            final HttpServletRequest request = ServletActionContext.getRequest();
            if (request != null)
            {
                String requestSortBy = request.getParameter("sortBy");
                if (requestSortBy != null)
                    sortBy = requestSortBy;
                String requestSortOrder = request.getParameter("sortOrder");
                if (requestSortOrder != null)
                    sortOrder = requestSortOrder;
            }
            if (sortBy == null) {
                sortBy = "date";
            }

            // the old versions are retrieved by the template, so we only want the latest versions in this list
            List<Attachment> attachments = getLatestVersionsOfAttachments(contentObject, sortBy, sortOrder);
            String fileNamePatterns = parameters.get("patterns"); // TODO what should this parameter be called?
            if (fileNamePatterns != null)
            {
                String[] patterns = fileNamePatterns.split(",");
                for (int i = 0; i < patterns.length; ++i)
                {
                	String pattern = patterns[i].trim();
                	// this is a common enough mistake 
                	if(pattern.startsWith("*"))
                	{
                		pattern = "."+pattern;
                	}
                		
                    Pattern p = Pattern.compile(pattern);
                    for (Attachment a : attachments)
                    {
                    	if (p.matcher(a.getFileName()).matches())
                    	{
                    		filteredList.add(a);
                    	}
                    }
                }
            }
            else
            {
                filteredList = attachments;
            }
            String labelsString = parameters.get("labels");
            final List<String> labels = !StringUtils.isBlank(labelsString) ?  Arrays.asList(labelsString.split(",")) : Collections.EMPTY_LIST;
            if(!labels.isEmpty())
            {
            	filteredList = Lists.newArrayList(Collections2.filter(filteredList, new Predicate<Attachment>() {
            		@Override
            		public boolean apply(Attachment input) {
            			
            			// we are only apply the predicate if the list of labels is non-empty 
            			if(input.getLabellings().isEmpty())
            				return false;
            			
            			for(String labelStr : labels)
            			{
            				labelStr = labelStr.trim();
            				boolean foundIt = false;
            				for(Label label : input.getLabels())
            				{
            					if(labelStr.equals(label.getName()))
            					{
            						foundIt = true;
            						break;
            					}
            				}
            				if(foundIt == false)
            					return false;
            			}
            			return true;
                    }
                }));
            }
        }

        User remoteUser = AuthenticatedUserThreadLocal.getUser();
        // now create a simple velocity context and render a template for the output
        Map<String, Object> contextMap = velocityHelperService.createDefaultVelocityContext();
        contextMap.put("latestVersionsOfAttachments", filteredList);
        
        Boolean hasAttachFilePermissions = false;
        try
        {
            hasAttachFilePermissions = permissionManager.hasCreatePermission(remoteUser, contentObject,
                    Attachment.class);
        }
        catch (IllegalStateException ex)
        {
            // the contentObject is probably a draft
        }

        boolean showAttachmentActions = !RenderContext.PDF.equals(conversionContext.getOutputType());

        contextMap.put("hasAttachFilePermissions", hasAttachFilePermissions);
        contextMap.put("page", contentObject);

        // the reference to this macro allows the template to call getAttachmentDetails() (see below)
        contextMap.put("macro", this);

        contextMap.put("old", getBooleanParameter(parameters, "old", true));
        contextMap.put("preview", getBooleanParameter(parameters, "preview", true));
        contextMap.put("upload", getBooleanParameter(parameters, "upload", true));
        // CONFATT-65 : File upload controls do not work when macro is displayed in macro browser & preview area. We will hide the controls when it's displayed
        //              in macro browser. However due to https://jira.atlassian.com/browse/CONF-24923, we are unable to fix this when macro is displayed in preview
        contextMap.put("renderedInPreview", RenderContext.PREVIEW.equals(conversionContext.getOutputType()));
        contextMap.put("max", 5);
        contextMap.put("remoteUser", remoteUser);
        contextMap.put("attachmentsMacroHelper", this);
        contextMap.put("showActions", showAttachmentActions);
        contextMap.put("outputType", conversionContext.getOutputType());
        contextMap.put("macroParams", getMacroParametersWithSortByParamReadFromRequest(parameters));
        contextMap.put("uploadIFrameName", RandomStringUtils.randomAlphanumeric(64));
        contextMap.put("sortBy", sortBy);
        contextMap.put("sortOrder", sortOrder);
        contextMap.put("newSortOrder", SORTORDER_DESCENDING.equals(sortOrder) ? SORTORDER_ASCENDING : SORTORDER_DESCENDING);
        ConfluenceUserPreferences pref = userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.getUser());
        contextMap.put("friendlyDateFormatter", new FriendlyDateFormatter(RequestTimeThreadLocal.getTimeOrNow(), pref.getDateFormatter(formatSettingsManager, localeManager)));

        contextMap.put("pdlEnabled", Long.parseLong(GeneralUtil.getBuildNumber()) >= 4000);
        contextMap.put("spaceKey", conversionContext.getSpaceKey());

        return velocityHelperService.getRenderedTemplate("templates/extra/attachments/attachmentsmacro.vm", contextMap);
    }

    public List<Attachment> getAllAttachmentVersions(Attachment attachment)
    {
        return attachmentManager.getAllVersions(attachment);
    }

    public boolean willBeRendered(final Attachment attachment)
    {
        return imagePreviewRenderer.willBeRendered(attachment);
    }

    public WebInterfaceContext getWebInterfaceContext(WebInterfaceContext context, Attachment attachment)
    {
        DefaultWebInterfaceContext defaultContext = new DefaultWebInterfaceContext(context);
        defaultContext.setAttachment(attachment);

        // When in the page gadget there is no action to retrieve the page from.
        if (contentObject instanceof AbstractPage || ConfluenceRenderContextOutputType.PAGE_GADGET.toString().equals(pageContext.getOutputType()))
        {
            defaultContext.setPage((AbstractPage) contentObject);
        }
        return defaultContext;
    }

    private Boolean getBooleanParameter(Map<String, String> parameters, String name, boolean defaultValue)
    {
        String parameterValue = parameters.get(name);
        if (parameterValue == null)
        {
            return defaultValue;
        }
        else
        {
            return Boolean.valueOf(parameterValue);
        }
    }

    private List<Attachment> getLatestVersionsOfAttachments(ContentEntityObject contentObject, String sortBy, String sortOrder)
    {
        List<Attachment> latestVersionsOfAttachments = attachmentManager.getLatestVersionsOfAttachments(contentObject);

        if (StringUtils.isNotBlank(sortBy))
        {
            Collections.sort(latestVersionsOfAttachments, new AttachmentComparator(sortBy, SORTORDER_DESCENDING.equals(sortOrder)));
        }
        return latestVersionsOfAttachments;
    }


    private Map<String, String> getMacroParametersWithSortByParamReadFromRequest(Map<String, String> macroParams)
    {
        Map<String, String> params = new HashMap<String, String>(macroParams);
        HttpServletRequest httpServletRequest = ServletActionContext.getRequest();

        if (null != httpServletRequest)
        {
            String sortBy = httpServletRequest.getParameter("sortBy");
            if (StringUtils.isNotBlank(sortBy))
                params.put("sortBy", sortBy);
        }

        return params;
    }

    /**
     * Copied from ViewAttachmentsAction
     */
    public String[] getAttachmentDetails(Attachment attachment)
    {
        return new String[]{GeneralUtil.escapeXml(attachment.getFileName()), String.valueOf(attachment.getVersion())};
    }
    
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        }
        catch (MacroExecutionException e)
        {
            throw new MacroException(e);
        }
    }

	public BodyType getBodyType() 
	{
		return BodyType.NONE;
	}

	public OutputType getOutputType() 
	{
		return OutputType.BLOCK;
	}
    
    private ContentEntityObject getPage(PageContext context, String pageTitleToRetrieve)
    {
        if (StringUtils.isBlank(pageTitleToRetrieve))
            return context.getEntity();

        String spaceKey = context.getSpaceKey();
        String pageTitle = pageTitleToRetrieve;

        int colonIndex = pageTitleToRetrieve.indexOf(":");
        if (colonIndex != -1 && colonIndex != pageTitleToRetrieve.length() - 1)
        {
            spaceKey = pageTitleToRetrieve.substring(0, colonIndex);
            pageTitle = pageTitleToRetrieve.substring(colonIndex + 1);
        }
        
        return pageManager.getPage(spaceKey, pageTitle);
    }
    
    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    @Override
    public ImagePlaceholder getImagePlaceholder(Map<String, String> stringStringMap, ConversionContext conversionContext) {
        return new DefaultImagePlaceholder(PLACEHOLDER_IMAGE_PATH, new Dimensions(310, 172), true);
    }
}
