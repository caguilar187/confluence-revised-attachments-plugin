<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.2</version>
    </parent>
    <groupId>com.atlassian.confluence.plugins</groupId>
    <artifactId>confluence-attachments-plugin</artifactId>
    <version>3.6.11-SNAPSHOT</version>

    <name>Confluence Attachments Plugin</name>
    <packaging>atlassian-plugin</packaging>

    <description>Macros to display attachments (useful when attachment display is off by default).</description>
    <url>https://studio.plugins.atlassian.com/wiki/display/CONFATT/Home</url>

    <properties>
        <atlassian.plugin.key>confluence.extra.attachments</atlassian.plugin.key>
        <jackson.version>1.9.1</jackson.version>
        <jersey.version>1.8</jersey.version>
        <atlassian.product.version>5.2-m14</atlassian.product.version>
        <atlassian.product.data.version>5.1</atlassian.product.data.version>
        <atlassian.product.test-lib.version>2.2.3</atlassian.product.test-lib.version>
        <jdkLevel>1.6</jdkLevel>
        <jvmArgs>-XX:MaxPermSize=256m -Xmx512m</jvmArgs>
        <unit.test.skip>false</unit.test.skip>
        <xvfb.enable>false</xvfb.enable>
        <xvfb.display>:0</xvfb.display>
        <http.port>1990</http.port>
        <http.confluence.port>${http.port}</http.confluence.port>
        <context.confluence.path>confluence</context.confluence.path>
        <baseurl.confluence>http://localhost:${http.confluence.port}/${context.confluence.path}</baseurl.confluence>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>4.1.4</version>
                <extensions>true</extensions>
                <configuration>
                    <jvmArgs>${jvmArgs}</jvmArgs>
                    <productVersion>${atlassian.product.version}</productVersion>
                    <productDataVersion>${atlassian.product.data.version}</productDataVersion>
                    <skipManifestValidation>true</skipManifestValidation>
                    <systemPropertyVariables>
                        <!-- This sets a system property required by the Functest RPC plugin to run in this environment -->
                        <confluence.version>${atlassian.product.version}</confluence.version>
                        <xvfb.enable>${xvfb.enable}</xvfb.enable>
                        <xvfb.display>${xvfb.display}</xvfb.display>
                        <http.port>${http.confluence.port}</http.port>
                        <baseurl.confluence>${baseurl.confluence}</baseurl.confluence>
                        <context.path>${context.confluence.path}</context.path>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${atlassian.product.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-webdriver-test</artifactId>
            <version>${atlassian.product.version}</version>
            <scope>test</scope>
            <exclusions>
                <!-- Exclude the newer version of HtmlUnit, since the plugin func-test-package needs to use the old one -->
                <exclusion>
                    <groupId>net.sourceforge.htmlunit</groupId>
                    <artifactId>htmlunit</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence.plugin</groupId>
            <artifactId>func-test-package</artifactId>
            <version>${atlassian.product.test-lib.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.8.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-asl</artifactId>
            <version>${jackson.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>${jackson.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-jaxrs</artifactId>
            <version>${jackson.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-xc</artifactId>
            <version>${jackson.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-server</artifactId>
            <version>${jersey.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-client</artifactId>
            <version>${jersey.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <scm>
        <connection>scm:git:git@bitbucket.org:atlassian/confluence-attachments-plugin.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:atlassian/confluence-attachments-plugin.git</developerConnection>
        <url>https://bitbucket.org/atlassian/confluence-attachments-plugin</url>
    </scm>

</project>
